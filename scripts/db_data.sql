--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-05-17 22:11:58 PYT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2248 (class 0 OID 78113)
-- Dependencies: 197
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auth_group (id, name) VALUES (1, 'Lider');
INSERT INTO auth_group (id, name) VALUES (2, 'Developer');
INSERT INTO auth_group (id, name) VALUES (3, 'ProductOwner');


--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 196
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 3, true);


--
-- TOC entry 2244 (class 0 OID 78093)
-- Dependencies: 193
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO django_content_type (id, name, app_label, model) VALUES (1, 'log entry', 'admin', 'logentry');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (2, 'permission', 'auth', 'permission');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (3, 'group', 'auth', 'group');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (4, 'user', 'auth', 'user');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (5, 'content type', 'contenttypes', 'contenttype');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (6, 'session', 'sessions', 'session');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (7, 'cliente', 'cliente', 'cliente');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (8, 'flujo', 'flujos', 'flujo');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (9, 'actividad', 'flujos', 'actividad');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (10, 'proyecto', 'proyecto', 'proyecto');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (11, 'equipo', 'proyecto', 'equipo');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (12, 'user_story', 'user_stories', 'user_story');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (13, 'historial', 'user_stories', 'historial');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (14, 'comentario', 'user_stories', 'comentario');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (15, 'sprint', 'sprint', 'sprint');


--
-- TOC entry 2246 (class 0 OID 78103)
-- Dependencies: 195
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (4, 'Can add permission', 2, 'add_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (5, 'Can change permission', 2, 'change_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (6, 'Can delete permission', 2, 'delete_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (7, 'Can add group', 3, 'add_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (8, 'Can change group', 3, 'change_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (9, 'Can delete group', 3, 'delete_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (10, 'Can add user', 4, 'add_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (11, 'Can change user', 4, 'change_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (12, 'Can delete user', 4, 'delete_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (13, 'Can add content type', 5, 'add_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (14, 'Can change content type', 5, 'change_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (15, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (16, 'Can add session', 6, 'add_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (17, 'Can change session', 6, 'change_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (18, 'Can delete session', 6, 'delete_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (19, 'Can add cliente', 7, 'add_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (20, 'Can change cliente', 7, 'change_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (21, 'Can delete cliente', 7, 'delete_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (22, 'Puede listar cliente', 7, 'listar_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (23, 'Puede crear cliente', 7, 'crear_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (24, 'Puede cambiar estado del cliente', 7, 'cambiar_estado_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (25, 'Puede modificar cliente', 7, 'modificar_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (26, 'Can add flujo', 8, 'add_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (27, 'Can change flujo', 8, 'change_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (28, 'Can delete flujo', 8, 'delete_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (29, 'Puede listar flujo', 8, 'listar_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (30, 'Puede crear flujo', 8, 'crear_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (31, 'Puede modificar flujo', 8, 'modificar_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (32, 'Puede eliminar flujo', 8, 'eliminar_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (33, 'Puede administrar flujos', 8, 'administrar_flujos');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (34, 'Puede ver flujos', 8, 'ver_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (35, 'Can add actividad', 9, 'add_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (36, 'Can change actividad', 9, 'change_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (37, 'Can delete actividad', 9, 'delete_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (38, 'Puede listar actividad', 9, 'listar_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (39, 'Puede crear actividad', 9, 'crear_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (40, 'Puede ver actividad', 9, 'ver_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (41, 'Puede eliminar actividad', 9, 'eliminar_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (42, 'Puede modificar actividad', 9, 'modificar_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (43, 'Can add proyecto', 10, 'add_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (44, 'Can change proyecto', 10, 'change_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (45, 'Can delete proyecto', 10, 'delete_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (46, 'Puede listar proyecto', 10, 'listar_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (47, 'Puede crear proyecto', 10, 'crear_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (48, 'Puede cambiar estado del proyecto', 10, 'cambiar_estado_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (49, 'Puede eliminar proyecto', 10, 'eliminar_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (50, 'Puede modificar detalle de proyecto', 10, 'modificar_detalle_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (51, 'Puede abrir proyecto', 10, 'abrir_proyecto');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (52, 'Can add equipo', 11, 'add_equipo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (53, 'Can change equipo', 11, 'change_equipo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (54, 'Can delete equipo', 11, 'delete_equipo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (55, 'Puede listar equipo', 11, 'listar_equipo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (56, 'Puede anhadir miembro', 11, 'anhadir_miembro');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (57, 'Puede quitar miembro', 11, 'quitar_miembro');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (58, 'Can add user_story', 12, 'add_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (59, 'Can change user_story', 12, 'change_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (60, 'Can delete user_story', 12, 'delete_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (61, 'Puede listar user story', 12, 'listar_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (62, 'Puede crear user story', 12, 'crear_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (63, 'Puede modificar user story', 12, 'modificar_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (64, 'Puede eliminar user story', 12, 'eliminar_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (65, 'Puede asignar flujo a user story', 12, 'asignar_flujo');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (66, 'puede registrar actividad de user story', 12, 'registrar_actividad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (67, 'Puede ver user story', 12, 'ver_user_story');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (68, 'Can add historial', 13, 'add_historial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (69, 'Can change historial', 13, 'change_historial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (70, 'Can delete historial', 13, 'delete_historial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (71, 'Can add comentario', 14, 'add_comentario');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (72, 'Can change comentario', 14, 'change_comentario');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (73, 'Can delete comentario', 14, 'delete_comentario');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (74, 'Can add sprint', 15, 'add_sprint');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (75, 'Can change sprint', 15, 'change_sprint');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (76, 'Can delete sprint', 15, 'delete_sprint');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (77, 'Puede iniciar sprint', 15, 'iniciar_sprint');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (78, 'Puede listar sprint', 15, 'listar_sprint');


--
-- TOC entry 2250 (class 0 OID 78123)
-- Dependencies: 199
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (1, 1, 1);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (2, 1, 2);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (3, 1, 3);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (4, 1, 4);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (5, 1, 5);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (6, 1, 6);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (7, 1, 7);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (8, 1, 8);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (9, 1, 9);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (10, 1, 10);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (11, 1, 11);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (12, 1, 12);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (13, 1, 13);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (14, 1, 14);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (15, 1, 15);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (16, 1, 16);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (17, 1, 17);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (18, 1, 18);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (19, 1, 19);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (20, 1, 20);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (21, 1, 21);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (22, 1, 22);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (23, 1, 23);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (24, 1, 24);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (25, 1, 25);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (26, 1, 26);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (27, 1, 27);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (28, 1, 28);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (29, 1, 29);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (30, 1, 30);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (31, 1, 31);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (32, 1, 32);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (33, 1, 33);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (34, 1, 34);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (35, 1, 35);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (36, 1, 36);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (37, 1, 37);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (38, 1, 38);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (39, 1, 39);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (40, 1, 40);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (41, 1, 41);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (42, 1, 42);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (43, 1, 43);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (44, 1, 44);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (45, 1, 45);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (46, 1, 46);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (47, 1, 47);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (48, 1, 48);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (49, 1, 49);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (50, 1, 50);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (51, 1, 51);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (52, 1, 52);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (53, 1, 53);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (54, 1, 54);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (55, 1, 55);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (56, 1, 56);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (57, 1, 57);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (58, 1, 58);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (59, 1, 59);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (60, 1, 60);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (61, 1, 61);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (62, 1, 62);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (63, 1, 63);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (64, 1, 64);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (65, 1, 65);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (66, 1, 66);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (67, 1, 67);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (68, 1, 68);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (69, 1, 69);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (70, 1, 70);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (71, 1, 71);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (72, 1, 72);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (73, 1, 73);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (74, 1, 74);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (75, 1, 75);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (76, 1, 76);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (77, 1, 77);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (78, 1, 78);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (79, 2, 1);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (80, 2, 2);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (81, 2, 3);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (82, 2, 4);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (83, 2, 5);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (84, 2, 6);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (85, 2, 7);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (86, 2, 8);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (87, 2, 9);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (88, 2, 10);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (89, 2, 11);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (90, 2, 12);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (91, 2, 13);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (92, 2, 14);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (93, 2, 15);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (94, 2, 16);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (95, 2, 17);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (96, 2, 18);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (97, 2, 19);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (98, 2, 20);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (99, 2, 21);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (100, 2, 22);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (101, 2, 23);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (102, 2, 24);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (103, 2, 25);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (104, 2, 26);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (105, 2, 27);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (106, 2, 28);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (107, 2, 29);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (108, 2, 30);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (109, 2, 31);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (110, 2, 32);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (111, 2, 33);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (112, 2, 34);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (113, 2, 35);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (114, 2, 36);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (115, 2, 37);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (116, 2, 38);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (117, 2, 39);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (118, 2, 40);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (119, 2, 41);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (120, 2, 42);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (121, 2, 43);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (122, 2, 44);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (123, 2, 45);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (124, 2, 46);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (125, 2, 47);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (126, 2, 48);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (127, 2, 49);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (128, 2, 50);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (129, 2, 51);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (130, 2, 52);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (131, 2, 53);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (132, 2, 54);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (133, 2, 55);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (134, 2, 56);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (135, 2, 57);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (136, 2, 58);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (137, 2, 59);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (138, 2, 60);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (139, 2, 61);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (140, 2, 62);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (141, 2, 63);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (142, 2, 64);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (143, 2, 65);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (144, 2, 66);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (145, 2, 67);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (146, 2, 68);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (147, 2, 69);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (148, 2, 70);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (149, 2, 71);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (150, 2, 72);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (151, 2, 73);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (152, 2, 74);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (153, 2, 75);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (154, 2, 76);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (155, 2, 77);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (156, 2, 78);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (157, 3, 1);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (158, 3, 2);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (159, 3, 3);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (160, 3, 4);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (161, 3, 5);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (162, 3, 6);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (163, 3, 7);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (164, 3, 8);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (165, 3, 9);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (166, 3, 10);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (167, 3, 11);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (168, 3, 12);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (169, 3, 13);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (170, 3, 14);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (171, 3, 15);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (172, 3, 16);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (173, 3, 17);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (174, 3, 18);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (175, 3, 19);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (176, 3, 20);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (177, 3, 21);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (178, 3, 22);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (179, 3, 23);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (180, 3, 24);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (181, 3, 25);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (182, 3, 26);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (183, 3, 27);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (184, 3, 28);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (185, 3, 29);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (186, 3, 30);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (187, 3, 31);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (188, 3, 32);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (189, 3, 33);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (190, 3, 34);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (191, 3, 35);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (192, 3, 36);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (193, 3, 37);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (194, 3, 38);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (195, 3, 39);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (196, 3, 40);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (197, 3, 41);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (198, 3, 42);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (199, 3, 43);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (200, 3, 44);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (201, 3, 45);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (202, 3, 46);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (203, 3, 47);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (204, 3, 48);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (205, 3, 49);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (206, 3, 50);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (207, 3, 51);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (208, 3, 52);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (209, 3, 53);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (210, 3, 54);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (211, 3, 55);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (212, 3, 56);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (213, 3, 57);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (214, 3, 58);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (215, 3, 59);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (216, 3, 60);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (217, 3, 61);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (218, 3, 62);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (219, 3, 63);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (220, 3, 64);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (221, 3, 65);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (222, 3, 66);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (223, 3, 67);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (224, 3, 68);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (225, 3, 69);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (226, 3, 70);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (227, 3, 71);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (228, 3, 72);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (229, 3, 73);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (230, 3, 74);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (231, 3, 75);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (232, 3, 76);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (233, 3, 77);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (234, 3, 78);


--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 198
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 234, true);


--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 194
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 78, true);


--
-- TOC entry 2252 (class 0 OID 78133)
-- Dependencies: 201
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (2, 'pbkdf2_sha256$15000$DpQEK4NPGpmR$8ViWYTDKjwexMMtfzAZn1+ddRdpjbnZjUMjhXlaIRmg=', '2015-05-17 19:25:21.168295-04', false, 'newuser', 'New User', 'Prueba', 'newuser@newuser.com', false, true, '2015-05-17 19:25:21.168295-04');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (3, 'pbkdf2_sha256$15000$nLrXKccmRSky$bkNFVsDyFVSVe6PglUSSjllYNtzNZKfZQL1vcX61UoY=', '2015-05-17 19:25:40.872679-04', false, 'hdmedina', 'Hernan', 'Medina', 'hdmedina.py@gmail.com', false, true, '2015-05-17 19:25:40.872679-04');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (4, 'pbkdf2_sha256$15000$PDbVUOi5iZYi$5aZXj0SjXshu2uxGM7agGCCp3SKOfFl6FEg+HRVLBD8=', '2015-05-17 19:26:04.760909-04', false, 'eduardo', 'Eduado', 'Me', 'edmendez@gmail.com', false, true, '2015-05-17 19:26:04.760909-04');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (5, 'pbkdf2_sha256$15000$ZJqqyFg2ltKQ$AuT4XXnX0e/wer2qQrDkvQQ+KvoQI379VZm7vcLfxnM=', '2015-05-17 19:26:29.063691-04', false, 'litatus', 'Miguel', 'Legal', 'legal.miguel@gmail.com', false, true, '2015-05-17 19:26:29.063691-04');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (6, 'pbkdf2_sha256$15000$QSgdrYgchaTu$+QuxUIodXh8wiaZ7oqzKvRgYozGfYuu3U9+Yu6VgJ7g=', '2015-05-17 19:26:59.104652-04', false, 'otrouser', 'New User', 'Prueba2', '02@hotmail.com', false, true, '2015-05-17 19:26:59.104652-04');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (1, 'pbkdf2_sha256$15000$LmOt3k1CAiYq$DsGyMZXYknUfcOxr3u2TQyyh6fZVHlAiPtR7LEnmxRY=', '2015-05-17 20:17:13.910722-04', true, 'admin', 'Miguel', 'Pereira', 'legal.miguel@gmail.com', true, true, '2015-03-30 18:55:14.396-04');


--
-- TOC entry 2254 (class 0 OID 78143)
-- Dependencies: 203
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (1, 5, 1);
INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (2, 4, 1);


--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 202
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 2, true);


--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 200
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 6, true);


--
-- TOC entry 2256 (class 0 OID 78153)
-- Dependencies: 205
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 204
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 2224 (class 0 OID 77911)
-- Dependencies: 173
-- Data for Name: cliente_cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO cliente_cliente (id, empresa, ruc, telefono, direccion, borrado_logico) VALUES (1, 'New Generation', '98791-0', 333520, 'consegui un mapa y te muestro', false);
INSERT INTO cliente_cliente (id, empresa, ruc, telefono, direccion, borrado_logico) VALUES (2, 'Konecta', '76576-7', 555558, 'que te importa, casi no se', false);


--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 172
-- Name: cliente_cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cliente_cliente_id_seq', 2, true);


--
-- TOC entry 2258 (class 0 OID 78207)
-- Dependencies: 207
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 206
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 192
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 15, true);


--
-- TOC entry 2222 (class 0 OID 77900)
-- Dependencies: 171
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO django_migrations (id, app, name, applied) VALUES (1, 'contenttypes', '0001_initial', '2015-05-17 19:20:54.910854-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (2, 'auth', '0001_initial', '2015-05-17 19:20:56.034418-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (3, 'admin', '0001_initial', '2015-05-17 19:20:56.369654-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (4, 'sessions', '0001_initial', '2015-05-17 19:20:56.647908-04');


--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 170
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 4, true);


--
-- TOC entry 2259 (class 0 OID 78229)
-- Dependencies: 208
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('qmm6inxd2mg9snpetnh0im52r7ebsq4k', 'MDc0YzE4MjA4MTMyNDg1YjM0YWQyYTM0NDYzODllYjhkZTcyZTNiYzp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyNjYxZjdkZTRmMDEwYTZjMjk3OGM3OGY2ZjY5MWU1OGUyODA3MjIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9', '2015-05-31 20:17:13.935351-04');


--
-- TOC entry 2230 (class 0 OID 77940)
-- Dependencies: 179
-- Data for Name: proyecto_proyecto; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO proyecto_proyecto (id, nombre, descripcion, fecha_ini, fecha_fin, estado, duracion_sprint, lider_id, observaciones, fecha_fin_real, cliente_id, borrado_logico) VALUES (1, 'Sistema Gerenciador de Proyectos', 'Sistema dedicado para el desarrollo de nuevos proyectos.', '2015-05-17', '2016-06-17', 'ACT', 14, 5, 'No existe ninguna observación al momento de la creación.', NULL, 1, false);
INSERT INTO proyecto_proyecto (id, nombre, descripcion, fecha_ini, fecha_fin, estado, duracion_sprint, lider_id, observaciones, fecha_fin_real, cliente_id, borrado_logico) VALUES (2, 'Proyecto PyMES', 'Sistema para Administrar Empresas Pequeñas y Medianas.', '2015-05-17', '2016-05-17', 'ACT', 21, 4, 'Proyecto RE-lanzamiento.', NULL, 2, false);


--
-- TOC entry 2226 (class 0 OID 77919)
-- Dependencies: 175
-- Data for Name: flujos_flujo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO flujos_flujo (id, nombre, borrado_logico, proyecto_id) VALUES (2, 'Desarrollo', false, 1);
INSERT INTO flujos_flujo (id, nombre, borrado_logico, proyecto_id) VALUES (1, 'Analisis', false, 1);
INSERT INTO flujos_flujo (id, nombre, borrado_logico, proyecto_id) VALUES (3, 'Primer Flujo de Prueba', false, 2);
INSERT INTO flujos_flujo (id, nombre, borrado_logico, proyecto_id) VALUES (4, 'Segundo Flujo', false, 2);


--
-- TOC entry 2228 (class 0 OID 77927)
-- Dependencies: 177
-- Data for Name: flujos_actividad; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (1, 1, 'Visita al Cliente', 1);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (2, 2, 'Req. Funcionales', 1);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (3, 3, 'Diagrama Clases', 1);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (4, 1, 'Analisis', 2);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (5, 2, 'Diseño', 2);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (6, 3, 'Implementación', 2);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (7, 1, 'Analisis', 3);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (8, 2, 'Disenho', 3);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (9, 3, 'Implementacion', 3);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (10, 1, 'Diseño', 4);
INSERT INTO flujos_actividad (id, orden, nombre, flujo_id) VALUES (11, 2, 'Implementacion', 4);


--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 176
-- Name: flujos_actividad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('flujos_actividad_id_seq', 11, true);


--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 174
-- Name: flujos_flujo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('flujos_flujo_id_seq', 4, true);


--
-- TOC entry 2232 (class 0 OID 77963)
-- Dependencies: 181
-- Data for Name: proyecto_equipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (1, 2, 3, 1);
INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (3, 3, 2, 1);
INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (4, 4, 2, 1);
INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (5, 5, 2, 1);
INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (6, 6, 3, 2);
INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (7, 3, 2, 2);
INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (8, 4, 2, 2);
INSERT INTO proyecto_equipo (id, miembro_id, rol_id, proyecto_id) VALUES (9, 5, 2, 2);


--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 180
-- Name: proyecto_equipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('proyecto_equipo_id_seq', 9, true);


--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 178
-- Name: proyecto_proyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('proyecto_proyecto_id_seq', 2, true);


--
-- TOC entry 2242 (class 0 OID 78056)
-- Dependencies: 191
-- Data for Name: sprint_sprint; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sprint_sprint (id, numero, fecha_ini, fecha_fin, estado, proyecto_id) VALUES (1, 1, '2015-05-12', '2015-05-26', 'FIN', 1);
INSERT INTO sprint_sprint (id, numero, fecha_ini, fecha_fin, estado, proyecto_id) VALUES (2, 2, '2015-05-11', '2015-05-25', 'ACT', 1);


--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 190
-- Name: sprint_sprint_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sprint_sprint_id_seq', 2, true);


--
-- TOC entry 2234 (class 0 OID 77976)
-- Dependencies: 183
-- Data for Name: user_stories_user_story; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO user_stories_user_story (id, nombre, descripcion, prioridad, valor_negocio, valor_tecnico, size, horas_dedicadas, proyecto_id, finalizado, borrado_logico, tiene_sprint, flujo_id, actividad_id, estado, usuario_asociado_id, usuario_modificacion_id, fecha_modificacion) VALUES (3, 'US-201505-11', 'Comenzar! Ya!', 100, 10, 10, 30, 3, 1, false, false, true, 2, 5, 'TODO', 3, 1, '2015-05-17 20:34:13-04');
INSERT INTO user_stories_user_story (id, nombre, descripcion, prioridad, valor_negocio, valor_tecnico, size, horas_dedicadas, proyecto_id, finalizado, borrado_logico, tiene_sprint, flujo_id, actividad_id, estado, usuario_asociado_id, usuario_modificacion_id, fecha_modificacion) VALUES (2, 'US-201504-02', 'Estructurar lo hablado.', 100, 10, 10, 30, 1, 1, false, false, true, 1, 1, 'DOING', 4, 1, '2015-05-17 20:33:26-04');
INSERT INTO user_stories_user_story (id, nombre, descripcion, prioridad, valor_negocio, valor_tecnico, size, horas_dedicadas, proyecto_id, finalizado, borrado_logico, tiene_sprint, flujo_id, actividad_id, estado, usuario_asociado_id, usuario_modificacion_id, fecha_modificacion) VALUES (1, 'US-201504-01', 'Visita al Cliente.', 100, 10, 10, 18, 0, 1, false, false, true, 1, 1, 'TODO', 5, 1, '2015-05-17 20:30:41-04');
INSERT INTO user_stories_user_story (id, nombre, descripcion, prioridad, valor_negocio, valor_tecnico, size, horas_dedicadas, proyecto_id, finalizado, borrado_logico, tiene_sprint, flujo_id, actividad_id, estado, usuario_asociado_id, usuario_modificacion_id, fecha_modificacion) VALUES (4, 'US-XXXX04-01', 'Una descripción más.', 100, 10, 10, 18, 0, 2, false, false, false, 3, 7, 'TODO', 7, 1, '2015-05-17 21:52:52-04');
INSERT INTO user_stories_user_story (id, nombre, descripcion, prioridad, valor_negocio, valor_tecnico, size, horas_dedicadas, proyecto_id, finalizado, borrado_logico, tiene_sprint, flujo_id, actividad_id, estado, usuario_asociado_id, usuario_modificacion_id, fecha_modificacion) VALUES (5, 'US-XXXX04-02', 'Y duuuuraaaaa.', 100, 10, 10, 18, 0, 2, false, false, false, 4, 10, 'TODO', 8, 1, '2015-05-17 21:53:35-04');
INSERT INTO user_stories_user_story (id, nombre, descripcion, prioridad, valor_negocio, valor_tecnico, size, horas_dedicadas, proyecto_id, finalizado, borrado_logico, tiene_sprint, flujo_id, actividad_id, estado, usuario_asociado_id, usuario_modificacion_id, fecha_modificacion) VALUES (6, 'US-XXXX04-05', 'Ya basta, verdad?!', 100, 10, 10, 30, 0, 2, false, false, false, 4, 10, 'TODO', 9, 1, '2015-05-17 21:54:16-04');


--
-- TOC entry 2240 (class 0 OID 78041)
-- Dependencies: 189
-- Data for Name: sprint_sprint_user_story; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sprint_sprint_user_story (id, sprint_id, user_story_id) VALUES (1, 1, 3);
INSERT INTO sprint_sprint_user_story (id, sprint_id, user_story_id) VALUES (2, 1, 2);
INSERT INTO sprint_sprint_user_story (id, sprint_id, user_story_id) VALUES (3, 1, 1);
INSERT INTO sprint_sprint_user_story (id, sprint_id, user_story_id) VALUES (4, 2, 3);
INSERT INTO sprint_sprint_user_story (id, sprint_id, user_story_id) VALUES (5, 2, 2);
INSERT INTO sprint_sprint_user_story (id, sprint_id, user_story_id) VALUES (6, 2, 1);


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 188
-- Name: sprint_sprint_user_story_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sprint_sprint_user_story_id_seq', 6, true);


--
-- TOC entry 2238 (class 0 OID 78025)
-- Dependencies: 187
-- Data for Name: user_stories_comentario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO user_stories_comentario (id, descripcion, user_story_id, fecha) VALUES (1, 'Trabaje 1 hora reloj. La empresa me debe mi combustible.', 2, '2015-05-17 20:46:29-04');
INSERT INTO user_stories_comentario (id, descripcion, user_story_id, fecha) VALUES (2, 'Trabajé Mucho ya.', 3, '2015-05-17 20:49:01-04');
INSERT INTO user_stories_comentario (id, descripcion, user_story_id, fecha) VALUES (3, 'Suficiente ya, no? Merezco algo mejor.', 3, '2015-05-17 20:49:28-04');
INSERT INTO user_stories_comentario (id, descripcion, user_story_id, fecha) VALUES (4, 'Aceptado por QA Linda =).', 3, '2015-05-17 20:50:30-04');


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 186
-- Name: user_stories_comentario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_stories_comentario_id_seq', 4, true);


--
-- TOC entry 2236 (class 0 OID 78009)
-- Dependencies: 185
-- Data for Name: user_stories_historial; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (1, 'User Story creado por admin en fecha 2015-05-17 20:30:41', 1);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (2, 'User Story creado por admin en fecha 2015-05-17 20:33:26', 2);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (3, 'User Story creado por admin en fecha 2015-05-17 20:34:13', 3);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (4, 'El usuario admin en fecha 2015-05-17 20:46:29 modifico: El estado, antes TODO, ahora DOING, Las horas dedicadas, antes 0, ahora 1, ', 2);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (5, 'El usuario admin en fecha 2015-05-17 20:49:01 modifico: El estado, antes TODO, ahora DOING, Las horas dedicadas, antes 0, ahora 1, ', 3);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (6, 'El usuario admin en fecha 2015-05-17 20:49:28 modifico: El estado, antes DOING, ahora DONE, Las horas dedicadas, antes 1, ahora 2, ', 3);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (7, 'El usuario admin en fecha 2015-05-17 20:50:30 modifico: La actividad, antes Analisis, ahora Diseño, La estado, antes DONE, ahora DONE, Las horas dedicadas, antes 2, ahora 3, ', 3);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (8, 'User Story creado por admin en fecha 2015-05-17 21:52:52', 4);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (9, 'User Story creado por admin en fecha 2015-05-17 21:53:35', 5);
INSERT INTO user_stories_historial (id, descripcion, user_story_id) VALUES (10, 'User Story creado por admin en fecha 2015-05-17 21:54:16', 6);


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 184
-- Name: user_stories_historial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_stories_historial_id_seq', 10, true);


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 182
-- Name: user_stories_user_story_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_stories_user_story_id_seq', 6, true);


-- Completed on 2015-05-17 22:11:58 PYT

--
-- PostgreSQL database dump complete
--

