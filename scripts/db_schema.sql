--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-05-17 22:11:21 PYT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE skp;
--
-- TOC entry 2225 (class 1262 OID 77897)
-- Name: skp; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE skp WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_PY.UTF-8' LC_CTYPE = 'es_PY.UTF-8';


ALTER DATABASE skp OWNER TO postgres;

\connect skp

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 209 (class 3079 OID 11789)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2228 (class 0 OID 0)
-- Dependencies: 209
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 78113)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 78111)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- TOC entry 2229 (class 0 OID 0)
-- Dependencies: 196
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- TOC entry 199 (class 1259 OID 78123)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 78121)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 2230 (class 0 OID 0)
-- Dependencies: 198
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- TOC entry 195 (class 1259 OID 78103)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 78101)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 194
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- TOC entry 201 (class 1259 OID 78133)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 78143)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 78141)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 202
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- TOC entry 200 (class 1259 OID 78131)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 200
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- TOC entry 205 (class 1259 OID 78153)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 78151)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 204
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- TOC entry 173 (class 1259 OID 77911)
-- Name: cliente_cliente; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cliente_cliente (
    id integer NOT NULL,
    empresa character varying(30),
    ruc character varying(10),
    telefono integer NOT NULL,
    direccion character varying(100),
    borrado_logico boolean NOT NULL
);


ALTER TABLE public.cliente_cliente OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 77909)
-- Name: cliente_cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cliente_cliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_cliente_id_seq OWNER TO postgres;

--
-- TOC entry 2235 (class 0 OID 0)
-- Dependencies: 172
-- Name: cliente_cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cliente_cliente_id_seq OWNED BY cliente_cliente.id;


--
-- TOC entry 207 (class 1259 OID 78207)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 78205)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- TOC entry 2236 (class 0 OID 0)
-- Dependencies: 206
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- TOC entry 193 (class 1259 OID 78093)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 78091)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- TOC entry 2237 (class 0 OID 0)
-- Dependencies: 192
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- TOC entry 171 (class 1259 OID 77900)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 77898)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2238 (class 0 OID 0)
-- Dependencies: 170
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- TOC entry 208 (class 1259 OID 78229)
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 77927)
-- Name: flujos_actividad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE flujos_actividad (
    id integer NOT NULL,
    orden integer NOT NULL,
    nombre character varying(50) NOT NULL,
    flujo_id integer NOT NULL
);


ALTER TABLE public.flujos_actividad OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 77925)
-- Name: flujos_actividad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE flujos_actividad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.flujos_actividad_id_seq OWNER TO postgres;

--
-- TOC entry 2239 (class 0 OID 0)
-- Dependencies: 176
-- Name: flujos_actividad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE flujos_actividad_id_seq OWNED BY flujos_actividad.id;


--
-- TOC entry 175 (class 1259 OID 77919)
-- Name: flujos_flujo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE flujos_flujo (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    borrado_logico boolean NOT NULL,
    proyecto_id integer NOT NULL
);


ALTER TABLE public.flujos_flujo OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 77917)
-- Name: flujos_flujo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE flujos_flujo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.flujos_flujo_id_seq OWNER TO postgres;

--
-- TOC entry 2240 (class 0 OID 0)
-- Dependencies: 174
-- Name: flujos_flujo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE flujos_flujo_id_seq OWNED BY flujos_flujo.id;


--
-- TOC entry 181 (class 1259 OID 77963)
-- Name: proyecto_equipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE proyecto_equipo (
    id integer NOT NULL,
    miembro_id integer NOT NULL,
    rol_id integer NOT NULL,
    proyecto_id integer NOT NULL
);


ALTER TABLE public.proyecto_equipo OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 77961)
-- Name: proyecto_equipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE proyecto_equipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proyecto_equipo_id_seq OWNER TO postgres;

--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 180
-- Name: proyecto_equipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE proyecto_equipo_id_seq OWNED BY proyecto_equipo.id;


--
-- TOC entry 179 (class 1259 OID 77940)
-- Name: proyecto_proyecto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE proyecto_proyecto (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion text NOT NULL,
    fecha_ini date NOT NULL,
    fecha_fin date NOT NULL,
    estado character varying(3) NOT NULL,
    duracion_sprint integer NOT NULL,
    lider_id integer NOT NULL,
    observaciones text NOT NULL,
    fecha_fin_real date,
    cliente_id integer NOT NULL,
    borrado_logico boolean NOT NULL
);


ALTER TABLE public.proyecto_proyecto OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 77938)
-- Name: proyecto_proyecto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE proyecto_proyecto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proyecto_proyecto_id_seq OWNER TO postgres;

--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 178
-- Name: proyecto_proyecto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE proyecto_proyecto_id_seq OWNED BY proyecto_proyecto.id;


--
-- TOC entry 191 (class 1259 OID 78056)
-- Name: sprint_sprint; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sprint_sprint (
    id integer NOT NULL,
    numero integer NOT NULL,
    fecha_ini date NOT NULL,
    fecha_fin date NOT NULL,
    estado character varying(3) NOT NULL,
    proyecto_id integer NOT NULL
);


ALTER TABLE public.sprint_sprint OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 78054)
-- Name: sprint_sprint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sprint_sprint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sprint_sprint_id_seq OWNER TO postgres;

--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 190
-- Name: sprint_sprint_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sprint_sprint_id_seq OWNED BY sprint_sprint.id;


--
-- TOC entry 189 (class 1259 OID 78041)
-- Name: sprint_sprint_user_story; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sprint_sprint_user_story (
    id integer NOT NULL,
    sprint_id integer NOT NULL,
    user_story_id integer NOT NULL
);


ALTER TABLE public.sprint_sprint_user_story OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 78039)
-- Name: sprint_sprint_user_story_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sprint_sprint_user_story_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sprint_sprint_user_story_id_seq OWNER TO postgres;

--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 188
-- Name: sprint_sprint_user_story_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sprint_sprint_user_story_id_seq OWNED BY sprint_sprint_user_story.id;


--
-- TOC entry 187 (class 1259 OID 78025)
-- Name: user_stories_comentario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_stories_comentario (
    id integer NOT NULL,
    descripcion text NOT NULL,
    user_story_id integer NOT NULL,
    fecha timestamp with time zone
);


ALTER TABLE public.user_stories_comentario OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 78023)
-- Name: user_stories_comentario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_stories_comentario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_stories_comentario_id_seq OWNER TO postgres;

--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 186
-- Name: user_stories_comentario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_stories_comentario_id_seq OWNED BY user_stories_comentario.id;


--
-- TOC entry 185 (class 1259 OID 78009)
-- Name: user_stories_historial; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_stories_historial (
    id integer NOT NULL,
    descripcion text NOT NULL,
    user_story_id integer NOT NULL
);


ALTER TABLE public.user_stories_historial OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 78007)
-- Name: user_stories_historial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_stories_historial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_stories_historial_id_seq OWNER TO postgres;

--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 184
-- Name: user_stories_historial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_stories_historial_id_seq OWNED BY user_stories_historial.id;


--
-- TOC entry 183 (class 1259 OID 77976)
-- Name: user_stories_user_story; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_stories_user_story (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion text,
    prioridad integer NOT NULL,
    valor_negocio integer NOT NULL,
    valor_tecnico integer NOT NULL,
    size integer NOT NULL,
    horas_dedicadas integer NOT NULL,
    proyecto_id integer NOT NULL,
    finalizado boolean NOT NULL,
    borrado_logico boolean NOT NULL,
    tiene_sprint boolean NOT NULL,
    flujo_id integer NOT NULL,
    actividad_id integer NOT NULL,
    estado character varying(5) NOT NULL,
    usuario_asociado_id integer NOT NULL,
    usuario_modificacion_id integer NOT NULL,
    fecha_modificacion timestamp with time zone
);


ALTER TABLE public.user_stories_user_story OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 77974)
-- Name: user_stories_user_story_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_stories_user_story_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_stories_user_story_id_seq OWNER TO postgres;

--
-- TOC entry 2247 (class 0 OID 0)
-- Dependencies: 182
-- Name: user_stories_user_story_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_stories_user_story_id_seq OWNED BY user_stories_user_story.id;


--
-- TOC entry 1993 (class 2604 OID 78116)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- TOC entry 1994 (class 2604 OID 78126)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 1992 (class 2604 OID 78106)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- TOC entry 1995 (class 2604 OID 78136)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- TOC entry 1996 (class 2604 OID 78146)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- TOC entry 1997 (class 2604 OID 78156)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 1981 (class 2604 OID 77914)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente_cliente ALTER COLUMN id SET DEFAULT nextval('cliente_cliente_id_seq'::regclass);


--
-- TOC entry 1998 (class 2604 OID 78210)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- TOC entry 1991 (class 2604 OID 78096)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- TOC entry 1980 (class 2604 OID 77903)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 1983 (class 2604 OID 77930)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flujos_actividad ALTER COLUMN id SET DEFAULT nextval('flujos_actividad_id_seq'::regclass);


--
-- TOC entry 1982 (class 2604 OID 77922)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flujos_flujo ALTER COLUMN id SET DEFAULT nextval('flujos_flujo_id_seq'::regclass);


--
-- TOC entry 1985 (class 2604 OID 77966)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proyecto_equipo ALTER COLUMN id SET DEFAULT nextval('proyecto_equipo_id_seq'::regclass);


--
-- TOC entry 1984 (class 2604 OID 77943)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proyecto_proyecto ALTER COLUMN id SET DEFAULT nextval('proyecto_proyecto_id_seq'::regclass);


--
-- TOC entry 1990 (class 2604 OID 78059)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sprint_sprint ALTER COLUMN id SET DEFAULT nextval('sprint_sprint_id_seq'::regclass);


--
-- TOC entry 1989 (class 2604 OID 78044)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sprint_sprint_user_story ALTER COLUMN id SET DEFAULT nextval('sprint_sprint_user_story_id_seq'::regclass);


--
-- TOC entry 1988 (class 2604 OID 78028)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_comentario ALTER COLUMN id SET DEFAULT nextval('user_stories_comentario_id_seq'::regclass);


--
-- TOC entry 1987 (class 2604 OID 78012)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_historial ALTER COLUMN id SET DEFAULT nextval('user_stories_historial_id_seq'::regclass);


--
-- TOC entry 1986 (class 2604 OID 77979)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_user_story ALTER COLUMN id SET DEFAULT nextval('user_stories_user_story_id_seq'::regclass);


--
-- TOC entry 2058 (class 2606 OID 78120)
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 2064 (class 2606 OID 78130)
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- TOC entry 2066 (class 2606 OID 78128)
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2060 (class 2606 OID 78118)
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2053 (class 2606 OID 78110)
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- TOC entry 2055 (class 2606 OID 78108)
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2075 (class 2606 OID 78148)
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2077 (class 2606 OID 78150)
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- TOC entry 2068 (class 2606 OID 78138)
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2081 (class 2606 OID 78158)
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2083 (class 2606 OID 78160)
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- TOC entry 2071 (class 2606 OID 78140)
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 2003 (class 2606 OID 77916)
-- Name: cliente_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cliente_cliente
    ADD CONSTRAINT cliente_cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 2087 (class 2606 OID 78216)
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2048 (class 2606 OID 78100)
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- TOC entry 2050 (class 2606 OID 78098)
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2001 (class 2606 OID 77908)
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 78236)
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 2009 (class 2606 OID 77932)
-- Name: flujos_actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY flujos_actividad
    ADD CONSTRAINT flujos_actividad_pkey PRIMARY KEY (id);


--
-- TOC entry 2005 (class 2606 OID 77924)
-- Name: flujos_flujo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY flujos_flujo
    ADD CONSTRAINT flujos_flujo_pkey PRIMARY KEY (id);


--
-- TOC entry 2019 (class 2606 OID 77968)
-- Name: proyecto_equipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proyecto_equipo
    ADD CONSTRAINT proyecto_equipo_pkey PRIMARY KEY (id);


--
-- TOC entry 2013 (class 2606 OID 77950)
-- Name: proyecto_proyecto_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proyecto_proyecto
    ADD CONSTRAINT proyecto_proyecto_nombre_key UNIQUE (nombre);


--
-- TOC entry 2016 (class 2606 OID 77948)
-- Name: proyecto_proyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proyecto_proyecto
    ADD CONSTRAINT proyecto_proyecto_pkey PRIMARY KEY (id);


--
-- TOC entry 2045 (class 2606 OID 78061)
-- Name: sprint_sprint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sprint_sprint
    ADD CONSTRAINT sprint_sprint_pkey PRIMARY KEY (id);


--
-- TOC entry 2039 (class 2606 OID 78046)
-- Name: sprint_sprint_user_story_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sprint_sprint_user_story
    ADD CONSTRAINT sprint_sprint_user_story_pkey PRIMARY KEY (id);


--
-- TOC entry 2042 (class 2606 OID 78048)
-- Name: sprint_sprint_user_story_sprint_id_user_story_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sprint_sprint_user_story
    ADD CONSTRAINT sprint_sprint_user_story_sprint_id_user_story_id_key UNIQUE (sprint_id, user_story_id);


--
-- TOC entry 2036 (class 2606 OID 78033)
-- Name: user_stories_comentario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_stories_comentario
    ADD CONSTRAINT user_stories_comentario_pkey PRIMARY KEY (id);


--
-- TOC entry 2033 (class 2606 OID 78017)
-- Name: user_stories_historial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_stories_historial
    ADD CONSTRAINT user_stories_historial_pkey PRIMARY KEY (id);


--
-- TOC entry 2025 (class 2606 OID 77986)
-- Name: user_stories_user_story_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_stories_user_story
    ADD CONSTRAINT user_stories_user_story_nombre_key UNIQUE (nombre);


--
-- TOC entry 2028 (class 2606 OID 77984)
-- Name: user_stories_user_story_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_stories_user_story
    ADD CONSTRAINT user_stories_user_story_pkey PRIMARY KEY (id);


--
-- TOC entry 2056 (class 1259 OID 78167)
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 2061 (class 1259 OID 78178)
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- TOC entry 2062 (class 1259 OID 78179)
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- TOC entry 2051 (class 1259 OID 78166)
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- TOC entry 2072 (class 1259 OID 78192)
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- TOC entry 2073 (class 1259 OID 78191)
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- TOC entry 2078 (class 1259 OID 78204)
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 2079 (class 1259 OID 78203)
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 2069 (class 1259 OID 78180)
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 2084 (class 1259 OID 78227)
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- TOC entry 2085 (class 1259 OID 78228)
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- TOC entry 2088 (class 1259 OID 78237)
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- TOC entry 2091 (class 1259 OID 78238)
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 2007 (class 1259 OID 78073)
-- Name: flujos_actividad_flujo_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX flujos_actividad_flujo_id ON flujos_actividad USING btree (flujo_id);


--
-- TOC entry 2006 (class 1259 OID 78072)
-- Name: flujos_flujo_proyecto_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX flujos_flujo_proyecto_id ON flujos_flujo USING btree (proyecto_id);


--
-- TOC entry 2017 (class 1259 OID 78077)
-- Name: proyecto_equipo_miembro_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX proyecto_equipo_miembro_id ON proyecto_equipo USING btree (miembro_id);


--
-- TOC entry 2020 (class 1259 OID 78079)
-- Name: proyecto_equipo_proyecto_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX proyecto_equipo_proyecto_id ON proyecto_equipo USING btree (proyecto_id);


--
-- TOC entry 2021 (class 1259 OID 78078)
-- Name: proyecto_equipo_rol_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX proyecto_equipo_rol_id ON proyecto_equipo USING btree (rol_id);


--
-- TOC entry 2010 (class 1259 OID 78076)
-- Name: proyecto_proyecto_cliente_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX proyecto_proyecto_cliente_id ON proyecto_proyecto USING btree (cliente_id);


--
-- TOC entry 2011 (class 1259 OID 78075)
-- Name: proyecto_proyecto_lider_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX proyecto_proyecto_lider_id ON proyecto_proyecto USING btree (lider_id);


--
-- TOC entry 2014 (class 1259 OID 78074)
-- Name: proyecto_proyecto_nombre_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX proyecto_proyecto_nombre_like ON proyecto_proyecto USING btree (nombre varchar_pattern_ops);


--
-- TOC entry 2046 (class 1259 OID 78090)
-- Name: sprint_sprint_proyecto_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sprint_sprint_proyecto_id ON sprint_sprint USING btree (proyecto_id);


--
-- TOC entry 2040 (class 1259 OID 78088)
-- Name: sprint_sprint_user_story_sprint_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sprint_sprint_user_story_sprint_id ON sprint_sprint_user_story USING btree (sprint_id);


--
-- TOC entry 2043 (class 1259 OID 78089)
-- Name: sprint_sprint_user_story_user_story_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sprint_sprint_user_story_user_story_id ON sprint_sprint_user_story USING btree (user_story_id);


--
-- TOC entry 2037 (class 1259 OID 78087)
-- Name: user_stories_comentario_user_story_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_comentario_user_story_id ON user_stories_comentario USING btree (user_story_id);


--
-- TOC entry 2034 (class 1259 OID 78086)
-- Name: user_stories_historial_user_story_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_historial_user_story_id ON user_stories_historial USING btree (user_story_id);


--
-- TOC entry 2022 (class 1259 OID 78083)
-- Name: user_stories_user_story_actividad_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_user_story_actividad_id ON user_stories_user_story USING btree (actividad_id);


--
-- TOC entry 2023 (class 1259 OID 78082)
-- Name: user_stories_user_story_flujo_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_user_story_flujo_id ON user_stories_user_story USING btree (flujo_id);


--
-- TOC entry 2026 (class 1259 OID 78080)
-- Name: user_stories_user_story_nombre_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_user_story_nombre_like ON user_stories_user_story USING btree (nombre varchar_pattern_ops);


--
-- TOC entry 2029 (class 1259 OID 78081)
-- Name: user_stories_user_story_proyecto_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_user_story_proyecto_id ON user_stories_user_story USING btree (proyecto_id);


--
-- TOC entry 2030 (class 1259 OID 78084)
-- Name: user_stories_user_story_usuario_asociado_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_user_story_usuario_asociado_id ON user_stories_user_story USING btree (usuario_asociado_id);


--
-- TOC entry 2031 (class 1259 OID 78085)
-- Name: user_stories_user_story_usuario_modificacion_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_stories_user_story_usuario_modificacion_id ON user_stories_user_story USING btree (usuario_modificacion_id);


--
-- TOC entry 2105 (class 2606 OID 78161)
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2106 (class 2606 OID 78168)
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2107 (class 2606 OID 78173)
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2111 (class 2606 OID 78198)
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2109 (class 2606 OID 78186)
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2108 (class 2606 OID 78181)
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2110 (class 2606 OID 78193)
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2112 (class 2606 OID 78217)
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2113 (class 2606 OID 78222)
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2093 (class 2606 OID 77933)
-- Name: flujos_actividad_flujo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flujos_actividad
    ADD CONSTRAINT flujos_actividad_flujo_id_fkey FOREIGN KEY (flujo_id) REFERENCES flujos_flujo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2095 (class 2606 OID 77969)
-- Name: proyecto_equipo_proyecto_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proyecto_equipo
    ADD CONSTRAINT proyecto_equipo_proyecto_id_fkey FOREIGN KEY (proyecto_id) REFERENCES proyecto_proyecto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2092 (class 2606 OID 77956)
-- Name: proyecto_id_refs_id_67e9657d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flujos_flujo
    ADD CONSTRAINT proyecto_id_refs_id_67e9657d FOREIGN KEY (proyecto_id) REFERENCES proyecto_proyecto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2094 (class 2606 OID 77951)
-- Name: proyecto_proyecto_cliente_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proyecto_proyecto
    ADD CONSTRAINT proyecto_proyecto_cliente_id_fkey FOREIGN KEY (cliente_id) REFERENCES cliente_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2103 (class 2606 OID 78067)
-- Name: sprint_id_refs_id_4e5df4ab; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sprint_sprint_user_story
    ADD CONSTRAINT sprint_id_refs_id_4e5df4ab FOREIGN KEY (sprint_id) REFERENCES sprint_sprint(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2104 (class 2606 OID 78062)
-- Name: sprint_sprint_proyecto_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sprint_sprint
    ADD CONSTRAINT sprint_sprint_proyecto_id_fkey FOREIGN KEY (proyecto_id) REFERENCES proyecto_proyecto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2102 (class 2606 OID 78049)
-- Name: sprint_sprint_user_story_user_story_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sprint_sprint_user_story
    ADD CONSTRAINT sprint_sprint_user_story_user_story_id_fkey FOREIGN KEY (user_story_id) REFERENCES user_stories_user_story(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2101 (class 2606 OID 78034)
-- Name: user_stories_comentario_user_story_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_comentario
    ADD CONSTRAINT user_stories_comentario_user_story_id_fkey FOREIGN KEY (user_story_id) REFERENCES user_stories_user_story(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2100 (class 2606 OID 78018)
-- Name: user_stories_historial_user_story_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_historial
    ADD CONSTRAINT user_stories_historial_user_story_id_fkey FOREIGN KEY (user_story_id) REFERENCES user_stories_user_story(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2098 (class 2606 OID 77997)
-- Name: user_stories_user_story_actividad_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_user_story
    ADD CONSTRAINT user_stories_user_story_actividad_id_fkey FOREIGN KEY (actividad_id) REFERENCES flujos_actividad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2097 (class 2606 OID 77992)
-- Name: user_stories_user_story_flujo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_user_story
    ADD CONSTRAINT user_stories_user_story_flujo_id_fkey FOREIGN KEY (flujo_id) REFERENCES flujos_flujo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2096 (class 2606 OID 77987)
-- Name: user_stories_user_story_proyecto_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_user_story
    ADD CONSTRAINT user_stories_user_story_proyecto_id_fkey FOREIGN KEY (proyecto_id) REFERENCES proyecto_proyecto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2099 (class 2606 OID 78002)
-- Name: user_stories_user_story_usuario_asociado_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_stories_user_story
    ADD CONSTRAINT user_stories_user_story_usuario_asociado_id_fkey FOREIGN KEY (usuario_asociado_id) REFERENCES proyecto_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-05-17 22:11:22 PYT

--
-- PostgreSQL database dump complete
--

