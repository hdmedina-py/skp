"""
Django settings for skp project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


#direccion de los Fixtures para los Tests
FIXTURE_DIRS = (
    os.path.join(BASE_DIR,  'fixtures'),
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'f%ji!!&33r*syfjj0wo+(^wu=b%v6#l%jxi29!4(z_964iy+0j'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'apps.cliente',
    'apps.flujos',
    'apps.proyecto',
    'apps.user_stories',
    'apps.sprint',


)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'skp.urls'

WSGI_APPLICATION = 'skp.wsgi.application'

DELETE_MESSAGE = 50

MESSAGE_TAGS = {
    DELETE_MESSAGE: 'deleted',
}


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

psycopg_ = 'django.db.backends.postgresql_psycopg2'
skpProd = 'skpProd'
postgres = 'postgres'
DATABASES = {
    'default': {
        'ENGINE': psycopg_,
        'NAME': skpProd,
        'USER': postgres,
        'PASSWORD': postgres,
        'HOST':'',
        'PORT':'',
    },
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-PY'

TIME_ZONE = 'America/Asuncion'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


STATIC_ROOT = '/var/www/html/skp/skp/static/' # Path to Static Dir
STATIC_URL = '/static/'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

""" Esta variable nos proporciona la ruta a archivos estaticos """
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR,'static'),
)


EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'admsistis2@gmail.com'
EMAIL_HOST_PASSWORD = 'admsist2'
DEFAULT_FROM_EMAIL = 'admsistis2@gmail.com'
