__author__ = 'litatus'

from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from apps.cliente.models import Cliente
from apps.cliente.views import administrarClientes, modificarCliente
from apps.cliente.views import cambiarEstado, clienteNuevo

class test_cliente(TestCase):
    """
    Para realizar todos los tests a las funciones de vistas de cliente

    :author: Miguel Pereira
    """
    #cargar los clientes
    fixtures = ['clientes.json'] + ['usuarios.json']

    def setUp(self):
        """ Inicializamos la variable factory que posteriormente nos permitira cargar
            un request para utilizarlo en las vista.
        """
        self.factory = RequestFactory()

    def test_eliminarCliente(self):
        """ Cargamos en objeto request con el requerimiento de la url que nos
            permite cambiar de estado del cliente, en la pantalla de administracion de clientes
        """

        request = self.factory.get('/adm_clientes')

        #Asignamos a la variable user el usuario admin, y es este el que realiza las solicitudes.
        self.user = User.objects.get(pk=1)
        request.user = self.user

        #establezco un cliente cualquiera
        id = '2'

        #verifico que el cliente existe
        clienteTest = Cliente.objects.filter(pk=id)
        self.assertTrue(clienteTest)

        #cambio el estado del cliente
        respuesta = cambiarEstado(request, id)

        #verifico que se haya cambiado el estado del cliente
        clienteTest = Cliente.objects.get(pk=id)
        self.assertFalse(clienteTest.is_active)

        print 'Test de Eliminacion de Cliente ejecutado exitosamente.'

    def test_clienteNuevo(self):
        """
        Crea una instancia de Cliente, mediante una solicitud GET
        """

        request = self.factory.get('/adm_clientes')

        #Asignamos a la variable user el usuario admin, y es este el que realiza las solicitudes.
        self.user = User.objects.get(pk=1)
        request.user = self.user

        #verifico que el usuario admin existe
        existeUsuario = User.objects.filter(pk=1)
        self.assertTrue(existeUsuario)

        #voy a la pagina de creacion de cliente con su formulario
        request = self.factory.post('/adm_clientes/nuevo/', {'Empresa': 'Codelabs', 'RUC': '5437-1', 'Usuario_Asociado': '1'})
        request.user = self.user

        #verifico que este en la pagina de "usuario creado con exito"
        respuesta = clienteNuevo(request)
        self.assertTrue(respuesta.status_code, 200)

        #busco el cliente creado para verificar que existe
        busqueda = Cliente.objects.get(empresa='Codelabs')

        #imprimo alguna informacion para verificar que se cargaron los datos
        print '\tDatos del Cliente Creado'
        print '\tEmpresa: ' + busqueda.empresa
        print '\tRUC: ' + busqueda.ruc
        print '\tUsuario Asociado :' + str(busqueda.usuario_asociado) + '\n'

        print 'Test de creacion de Cliente ejecutado exitosamente.'

    def test_administrarCliente(self):
        """ Cargamos en objeto request con el requerimiento de la url de
        la pantalla de administracion de clientes y verificamos y muestra la lista de Clientes
        existentes en el sistema
        """
        request = self.factory.get('/adm_clientes')

        #Asignamos a la variable user el usuario admin, y es este el que realiza las solicitudes.
        self.user = User.objects.get(pk=1)
        request.user = self.user

        #verifico que vaya a la Administracion de Cliente y muestre la lista
        respuesta = administrarClientes(request)
        self.assertTrue(respuesta.status_code, 200)
        print 'Test de Administracion de Clientes ejecutado exitosamente.'

    def testClienteModificado(self):
        """
        Para la verificacion de la modificacion de un cliente existente en la Base de Datos
        """
        request = self.factory.get('/adm_clientes')

        #Asignamos a la variable user el usuario admin, y es este el que realiza las solicitudes.
        self.user = User.objects.get(pk=1)
        request.user = self.user

        #verifico que el usuario admin existe
        existeUsuario = User.objects.filter(pk=1)
        self.assertTrue(existeUsuario)

        #voy a la pagina de Modificar Cliente con su formulario
        request = self.factory.post('/adm_clientes/modificar/', {'Empresa': 'XXXX', 'RUC': '8888-8', 'Usuario_Asociado': '2'})
        request.user = self.user

        #busco el Cliente creado ultimo para mejor visualizacion
        clienteModificar = Cliente.objects.get(empresa='Konecta')
        id = clienteModificar.pk

        #imprimo para ver la informacion anterior y mas adelante la cambiada
        print '\tDatos del Cliente Existente'
        print '\tID: ' + str(id)
        print '\tEmpresa: ' + clienteModificar.empresa
        print '\tRUC: ' + clienteModificar.ruc
        print '\tUsuario Asociado :' + str(clienteModificar.usuario_asociado) + '\n'

        #verifico que este en la pagina de "Usuario Modificado con exito"
        respuesta = modificarCliente(request, id)
        self.assertTrue(respuesta.status_code, 200)

        #busco el cliente Modificado para verificar que se cambiaron sus valores anteriores
        busqueda = Cliente.objects.get(empresa='XXXX')

        #imprimo alguna informacion para verificar que se cargaron los datos
        print '\tDatos del Cliente Modificado'
        print '\tID: ' + str(busqueda.pk)
        print '\tEmpresa: ' + busqueda.empresa
        print '\tRUC: ' + busqueda.ruc
        print '\tUsuario Asociado :' + str(busqueda.usuario_asociado) + '\n'

        #imprimo todos los clientes

        print 'Test de Modificacion de Cliente ejecutado exitosamente.'
