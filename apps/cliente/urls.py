__author__ = 'litatus'
from django.conf.urls import patterns, url
from .views import clienteNuevo, verCliente, modificarCliente
from .views import administrarClientes, cambiarEstado

urlpatterns = patterns('',
        url(r'^adm_clientes/$', administrarClientes),
        url(r'^adm_clientes/nuevo/$', clienteNuevo),
        url(r'^adm_clientes/modificar/(?P<id_cliente>\d+)/$', modificarCliente),
        url(r'^adm_clientes/ver/(?P<id_cliente>\d+)/$', verCliente),
        url(r'^adm_clientes/cambiarEstado/(?P<id_cliente>.*)/$', cambiarEstado),

)