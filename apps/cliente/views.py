__author__ = 'litatus'
from django.views.generic import TemplateView
from django.contrib.auth.models import User, Permission
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template.context import RequestContext
from django.contrib.auth.hashers import check_password, make_password
from forms import ClienteNuevoForm, ClienteModificadoForm
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from collections import OrderedDict
from apps.cliente.models import Cliente

@login_required(login_url='/login/')
def administrarClientes(request):
    """
    Recibe un request, obtiene la lista de todos los clientes del sistema y
    luego retorna el html renderizado con la lista de clientes

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: clientes.html, donde se listan los usuarios, ademas de las funcionalidades para un usuario

    :author: Miguel Pereira
    """
    lista_clientes = Cliente.objects.filter(borrado_logico = False).order_by('id')
    ctx = {'lista_clientes':lista_clientes}

    return render_to_response('Cliente/clientes.html', ctx, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def clienteNuevo(request):
    """
    Recibe un request, muestra el formulario de creacion de cliente nuevo, verifica la integridad
    de los datos cargados, crea y guarda el cliente nuevo.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: clientecreado.html, donde se muestra un mensaje de creacion exitosa

    :author: Miguel Pereira
     """
    if request.method == 'POST':
        form = ClienteNuevoForm(request.POST)

        if form.is_valid():                             #verfica si tiene errores el formulario
            form.clean()
            empresa = form.cleaned_data['Empresa']
            ruc = form.cleaned_data['RUC']
            telefono = form.cleaned_data['Telefono']
            direccion=form.cleaned_data['Direccion']

            clienteNuevo = Cliente(empresa=empresa, ruc=ruc, direccion=direccion,telefono=telefono)
            clienteNuevo.save()

            template_name = './Cliente/clientecreado.html'
            return render(request, template_name)


    else:
        form = ClienteNuevoForm()

    template_name = './Cliente/clientenuevo.html'
    return render(request, template_name, {'form': form})

@login_required(login_url='/login/')
def verCliente(request, id_cliente):
    """
    Recibe un request y la clave del cliente, para lostrar los datos existentes de ese cliente en la Base de Datos.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: consultar_cliente.html, donde se muestran los datos correspondientes

    :author: Miguel Pereira
     """
    template_name = './Cliente/consultar_cliente.html'
    cliente = Cliente.objects.get(pk=id_cliente)

    return render(request, template_name, {'cliente': cliente, 'id_cliente': id_cliente})

@login_required(login_url='/login/')
def cambiarEstado(request, id_cliente):
    """
    Recibe un request y la clave del cliente, para cambiar el estado logico de dicho cliente.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: adm_cliente.html, donde se vuelve a la pantalla de administracion con el cambio hecho

    :author: Miguel Pereira
     """
    eliminacionLogica = Cliente.objects.get(pk=id_cliente)
    if not eliminacionLogica.borrado_logico:

        eliminacionLogica.borrado_logico = True
        eliminacionLogica.save()
        return HttpResponseRedirect('/adm_clientes/')

    else:
        eliminacionLogica.is_active = False
        eliminacionLogica.save()
        return HttpResponseRedirect('/adm_clientes/')

@login_required(login_url='/login/')
def modificarCliente(request, id_cliente):
    """
    Recibe un request y la clave del cliente, para mostrar la pantalla con los campos editables y pre-cargados
    con la informacion existente en la Base de Datos para ese cliente.
    Adicionalmente guarda el id del usuario que realizo las modificaciones.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: modificar_cliente.html, donde se ven los campos modificables

    :author: Miguel Pereira
     """

    modificador = User.objects.get(username=request.user.username)

    clienteModificado = Cliente.objects.get(id=id_cliente)

    if request.method == 'POST':
        form = ClienteModificadoForm(request.POST)

        if form.is_valid():
            form.clean()
            empresa = form.cleaned_data['Empresa']
            ruc = form.cleaned_data['RUC']
            telefono = form.cleaned_data['Telefono']
            direccion=form.cleaned_data['Direccion']

            clienteModificado.pk = id_cliente
            clienteModificado.empresa = empresa
            clienteModificado.ruc = ruc
            clienteModificado.telefono = telefono
            clienteModificado.direccion = direccion
            clienteModificado.save()

            template_name = './Cliente/cliente_modificado.html'
            return render(request, template_name)
    else:
        data = {'Empresa': clienteModificado.empresa, 'RUC': clienteModificado.ruc,
                'Telefono': clienteModificado.telefono, 'Direccion':clienteModificado.direccion}
        form = ClienteModificadoForm(data)
    template_name = './Cliente/modificar_cliente.html'
    return render(request, template_name, {'form': form, 'id_cliente': id_cliente})


