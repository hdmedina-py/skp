from django.contrib.auth.models import User
from apps.proyecto.models import Proyecto, Equipo

__author__ = 'eduardo'
from apps.flujos.models import Flujo, Actividad
from django.db import models

ESTADOS = (

    ('TODO', 'To Do'),
    ('DOING', 'Doing'),
    ('DONE', 'Done')
)




class User_Story(models.Model):
    """
    Contiene los campos de la tabla User_Story en la Base de Datos.
    El nombre del User_Story.
    Una descripcion sobre el User_Story.
    Un nivel de prioridad que va del 0 al 100 donde el es de mayor relevancia.
    Un valor de negocio que asigna la importancia del User Story para el cliente
    Un valor tecnico que asigna la importancia del User Story para el equipo
    Un size que significa la cantidad de horas estimadas para la realizacion de un User_Story
    Un valor de horas dedicadas que significa la cantidad de horas utilizadas para la realizacion de un User_Story
    El id del proyecto al que corresponde.
    Un boolean finalizado que sirve para identificar un User_Story que culmino con todas sus etapas
    Un borrado_logico que se utiliza para simular la eliminacion en la base de datos
    El id del flujo al que corresponde.
    El id de la actividad en la que se encuentra.
    Un ENUM de estados para saber en que estado del proceso se encuentra.
    Un usuario_asociado al que le corresponde el desarrollo del HU
    Un usuario y una fecha de modificacion que se utilizan para generar posteriormente el Historial de Cambios en el User_Story


    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=500, help_text='Introduzca una descripcion de HU', null=True)
    prioridad = models.IntegerField(help_text='Ingresar la prioridad del HU creado, un valor entre 0-100')
    valor_negocio = models.IntegerField(help_text='Ingresar el valor de negocio del HU, un valor entre 0-10')
    valor_tecnico = models.IntegerField(help_text='Ingresar el valor tecnico del HU, un valor entre 0-10')
    size = models.IntegerField(help_text='Ingresar el size del HU, en horas')
    horas_dedicadas = models.IntegerField(default=0,help_text='Ingresar las horas dedicadas al HU')
    proyecto = models.ForeignKey(Proyecto)
    finalizado = models.BooleanField(default=False)
    borrado_logico = models.BooleanField(default=False)
    tiene_sprint = models.BooleanField(default=False)

    flujo = models.ForeignKey(Flujo)
    actividad = models.ForeignKey(Actividad)
    estado= models.CharField(max_length=5, choices= ESTADOS)
    usuario_asociado = models.ForeignKey(Equipo)

    usuario_modificacion = models.ForeignKey(User, related_name='Usuario Modificador')
    fecha_modificacion = models.DateTimeField(help_text='Fecha de modificacion del HU', null=True)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'user_story'
        verbose_name_plural = 'user_storys'
        permissions = (
            ('listar_user_story', 'Puede listar user story'),
            ('crear_user_story', 'Puede crear user story'),
            ('modificar_user_story', 'Puede modificar user story'),
            ('eliminar_user_story', 'Puede eliminar user story'),
            ('asignar_flujo', 'Puede asignar flujo a user story'),
            ('registrar_actividad', 'puede registrar actividad de user story'),
            ('ver_user_story', 'Puede ver user story'),
        )
class Historial(models.Model):
    """
    Contiene los campos de la tabla Nota en la Base de Datos. Que esta relacionado a los cambios que se hacen dentro de un User_Story
    El campo descripcion guarda una concatenacion de los cambios realizados por el usuario dentro de un User_Story
    El campo user_story sirve para referenciar la nota con el User_story al que corresponde

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    descripcion = models.TextField(max_length=400)
    user_story = models.ForeignKey(User_Story)

class Comentario(models.Model):
    """
    Contiene los campos de la tabla Comentario en la Base de Datos.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    descripcion = models.TextField(max_length=200)
    user_story = models.ForeignKey(User_Story)
    fecha = models.DateTimeField(help_text='Fecha de creacion del comentario', null=True)
    horas_dedicadas= models.IntegerField(default=0,help_text='Ingresar las horas dedicadas al HU')

