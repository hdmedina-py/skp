from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.auth.models import User
from apps.flujos.models import Flujo,Actividad
from apps.proyecto.models import Equipo
from apps.user_stories.models import ESTADOS

__author__ = 'eduardo'
from django import forms
from django.utils.datetime_safe import datetime


ESTADOS = (

    ('TODO', 'To Do'),
    ('DOING', 'Doing'),
    ('DONE', 'Done')
)
today = datetime.now() #fecha actual
dateFormat = today.strftime("%Y-%m-%d") # fecha con format

class UserStoryNuevoForm(forms.Form):
    """
    Formulario que se utiliza cuando se carga un nuevo User_Story

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    Nombre = forms.CharField(widget=forms.TextInput(), max_length=20, required=True, error_messages={'required':'Ingrese un nombre para el HU', 'max_length':'Longitud maxima 20'})
    Descripcion = forms.CharField(widget=forms.Textarea(), required=True, max_length=300, error_messages={'required':'Ingrese una descripcion para el HU', 'max_length':'Longitud maxima 300'})
    Prioridad = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'Entre 1 y 100'}),max_value=100, min_value=0 , error_messages={'required':'Ingrese un valor de prioridad','min_value': 'Ingrese un valor mayor a 0'})
    Valor_De_Negocio = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'Entre 1 y 10'}),max_value= 10,min_value=0, error_messages={'max_value':'Ingrese un valor menor a 10','min_value': 'Ingrese un valor mayor a 0'})
    Valor_Tecnico = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'En 1 y 10'}),max_value= 10,min_value=0, error_messages={'max_value':'Ingrese un valor menor a 100', 'min_value': 'Ingrese un valor mayor a 0'})
    Size = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'En dias'}),min_value=0, error_messages={'min_value':'Ingrese un valor mayor a  0'})
    Usuario_Asociado = forms.ModelChoiceField(queryset=Equipo.objects.none(), required=True, error_messages={'required': 'Seleccione un Usuario', })
    Flujo = forms.ModelChoiceField(required=True, queryset=Flujo.objects.none(), error_messages={'required':'Ingrese un flujo para el HU'})

    def __init__(self, proyecto, *args, **kwargs):
        super(UserStoryNuevoForm, self).__init__(*args, **kwargs)

        self.fields['Flujo'].queryset = Flujo.objects.filter(proyecto=proyecto, borrado_logico = False)

        self.fields['Usuario_Asociado'].queryset = Equipo.objects.filter(proyecto=proyecto)


class UserStoryModificadoForm(forms.Form):
    Nombre = forms.CharField(widget=forms.TextInput(), max_length=20, required=True, error_messages={'required':'Ingrese un nombre para el HU', 'max_length':'Longitud maxima 20'})
    Descripcion = forms.CharField(widget=forms.Textarea(), required=True, max_length=300, error_messages={'required':'Ingrese una descripcion para el HU', 'max_length':'Longitud maxima 300'})
    Prioridad = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'Entre 1 y 100'}),max_value=100, min_value=0 , error_messages={'required':'Ingrese un valor de prioridad','min_value': 'Ingrese un valor mayor a 0'})
    Valor_De_Negocio = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'Entre 1 y 10'}),max_value= 10,min_value=0, error_messages={'max_value':'Ingrese un valor menor a 10','min_value': 'Ingrese un valor mayor a 0'})
    Valor_Tecnico = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'Entre 1 y 10'}),max_value= 10,min_value=0, error_messages={'max_value':'Ingrese un valor menor a 100', 'min_value': 'Ingrese un valor mayor a 0'})
    Size = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'En dias'}),min_value=0, error_messages={'min_value':'Ingrese un valor mayor a  0'})
    Usuario_Asociado = forms.ModelChoiceField(queryset=Equipo.objects.none(), required=True, error_messages={'required': 'Seleccione un Usuario', })

    #Flujo = forms.ModelChoiceField(required=True, queryset=Flujo.objects.none(), error_messages={'required':'Ingrese un flujo para el HU'})
    #Actividad = forms.ModelChoiceField(required=True, queryset=Actividad.objects.none())
    #Estado=forms.CharField(max_length=5,widget=forms.Select(choices= ESTADOS))

    def __init__(self, proyecto, *args, **kwargs):
        super(UserStoryModificadoForm, self).__init__(*args, **kwargs)

        self.fields['Usuario_Asociado'].queryset = Equipo.objects.filter(proyecto=proyecto)


class asignarFlujoForm(forms.Form):

    Flujo_Actual = forms.ModelChoiceField(required=True, queryset=Flujo.objects.none(), error_messages={'required':'Ingrese un flujo para el HU'})


    def __init__(self, proyecto, *args, **kwargs):
        super(asignarFlujoForm, self).__init__(*args, **kwargs)

        self.fields['Flujo_Actual'].queryset = Flujo.objects.filter(proyecto=proyecto, borrado_logico = False)

class actualizarProgresoForm(forms.Form):
    Estado=forms.CharField(max_length=5, widget=forms.Select(choices=ESTADOS))
    Actividad = forms.ModelChoiceField(required=True, queryset=Actividad.objects.none())
    Horas_Dedicadas = forms.IntegerField()
    Comentario = forms.CharField(widget=forms.Textarea(), required=True, max_length=300, error_messages={'required':'Ingrese un comentario para el HU'})


    def __init__(self,flujo, *args, **kwargs):
        super(actualizarProgresoForm, self).__init__(*args, **kwargs)

        self.fields['Actividad'].queryset = Actividad.objects.filter(flujo=flujo)
