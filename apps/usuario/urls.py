__author__ = 'eduardo'
from django.conf.urls import patterns, url
from .views import usuarioNuevo, cambiarEstado
from .views import modificarUsuario, verUsuario, administrarUsuarios

urlpatterns = patterns('',
        url(r'^adm_usuarios/$', administrarUsuarios),
        url(r'^adm_usuarios/nuevo/$', usuarioNuevo),
        url(r'^adm_usuarios/modificar/(?P<id_usuario>\d+)/$', modificarUsuario),
        url(r'^adm_usuarios/ver/(?P<id_usuario>\d+)/$', verUsuario),
        url(r'^adm_usuarios/cambiarEstado/(?P<id_usuario>.*)/$', cambiarEstado),

)
