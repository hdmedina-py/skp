__author__ = 'eduardo'

from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from apps.usuario.views import administrarUsuarios, modificarUsuario
from apps.usuario.views import cambiarEstado, usuarioNuevo

class test_user(TestCase):
    """    Cargamos los usuarios de prueba en la base de datos    """
    fixtures = ['usuarios.json']

    def setUp(self):
        """ Inicializamos la variable factory que posteriormente nos permitira cargar
            un request para utilizarlo en las vista.
        """
        self.factory = RequestFactory()


    def test_usuarioEliminar(self):
        """ Cargamos en objeto request con el requerimiento de la url que nos
            permite eliminar un usuario.
        """
        request = self.factory.get('/adm_usuarios/cambiarEstado/')

        #Asignamos a la variable user el usuario administrador.
        self.user = User.objects.get(pk=1)

        #Logueamos al usuario mediante el request
        request.user = self.user

        #Nos aseguramos que el usuario con id existe.
        id = '2'
        usuarioname = User.objects.get(pk=id)
        self.assertTrue(usuarioname)

        #Borramos al usuario con id 2
        response = cambiarEstado(request, id)

        #Nos aseguramos que el usuario con id se ha eliminado
        usuarioname = User.objects.get(pk=id)
        self.assertFalse(usuarioname.is_active)

        print 'Test de eliminacion de Usuario normal ejecutado exitosamente.'

        """ Ahora nos aseguramos de que el Administrador no puede ser eliminado
            utilizando la tecnica anterior.
        """
        id = '1'
        usuarioname = User.objects.get(pk=id)
        self.assertTrue(usuarioname)
        response = cambiarEstado(request, '1')

        """ Comprobamos que el administrador no puede ser eliminado.
        """
        usuarioname = User.objects.get(pk=id)
        self.assertTrue(usuarioname)
        print 'Test de eliminacion de Usuario Administrador ejecutado exitosamente.'

    def test_usuarioNuevo(self):

        # Crea una instancia de una solicitud GET
        self.user = User.objects.get(pk=1)

        #completamos en el request el formulario
        request = self.factory.post('/adm_usuarios/nuevo/', {'Usuario': 'lalala', 'Contrasenha': 'lalalapass', 'Reingresar_Contrasenha': 'lalalapass', 'Email': 'lalala@pol.una.py', 'Nombre': 'Hernan', 'Apellido': 'Medina'})
        request.user = self.user
        response = usuarioNuevo(request)                #mando a la vista de creacion de nuevo usuario
        self.assertEqual(response.status_code, 200)     #verifico que se haya recibido la pagina
        busco = User.objects.get(username='lalala')     #busco el usuario creado
        print 'Usuario Creado: '
        print '\tUsuario: ' + busco.username
        print '\tNombre: ' + busco.first_name
        print '\tApellido: ' + busco.last_name
        print 'Test de Creacion de Usuario ejecutado exitosamente.\n'

    def test_administrarUsuario(self):

        self.user = User.objects.get(pk=1)
        request = self.factory.get('/adm_usuarios/')
        request.user = self.user
        response = administrarUsuarios(request)         #Pasamos a la pagina de administracion de usuarios
        self.assertEqual(response.status_code, 200)
        print 'Test de Administracion de Usuarios ejecutado exitosamente.'

    def test_modificarUsuario(self):

        self.user = User.objects.get(pk=1)
        print ''
        print User.objects.all()

        id = '2'                                #busco un usuario para modificar

        request = self.factory.post('/adm_usuarios/modificar/', {'Usuario': 'MODlitatusMOD', 'Contrasenha': '12345', 'Reingresar_Contrasenha': '12345', 'Email': 'algo@algo.com.py', 'Nombre': 'No se', 'Apellido': 'quien sabe'})
        request.user = self.user
        response = modificarUsuario(request, id)
        self.assertEqual(response.status_code, 200)
        print User.objects.all()
        print 'Test de modificacion de usuario ejecutado exitosamente.'


    if __name__ == '__main__':
        unittest.main()