__author__ = 'eduardo'
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.core import validators
from django.core.exceptions import ValidationError

def validate_username_unique(value):
    if User.objects.filter(username=value).exists():
        raise ValidationError(u'El usuario ya existe en la base de datos')

class UsuarioNuevoForm (forms.Form):

    """

     Estos son los campos que se cargan durante el registro de un usuario
        que luego pasa al template html que recibe los datos para luego persistir
        Control de datos ingresados por el usuario.


    """

    Usuario = forms.CharField(widget=forms.TextInput(), validators=[validate_username_unique], max_length=14, min_length=5, required=True, error_messages={'required': 'Ingrese un nombre de usuario', 'max_length': 'Longitud maxima: 14 caracteres', 'min_length': 'Longitud minima: 5 caracteres'})
    Contrasenha = forms.CharField(widget=forms.PasswordInput(render_value=False), max_length=14, min_length=5, required=True, error_messages={'required': 'Ingrese contrasenha', 'max_length': 'Longitud maxima: 14', 'min_length': 'Longitu minima: 5 caracteres',})
    Reingresar_Contrasenha = forms.CharField(widget=forms.PasswordInput(render_value=False), max_length=14, min_length=5, required=True, error_messages={'required': 'Ingrese contrasenha', 'max_length': 'Longitud maxima: 14', 'min_length': 'Longitu minima: 5 caracteres',})
    Email = forms.CharField(widget=forms.TextInput(), required=False)
    Nombre = forms.CharField(widget=forms.TextInput(), max_length=30, required=True, error_messages={'required': 'Ingrese nombre', })
    Apellido = forms.CharField(widget=forms.TextInput(), max_length=30, required=True, error_messages={'required': 'Ingrese Apellido', })
    Super_Usuario = forms.BooleanField(required=False)
    def clean(self):
        super(forms.Form,self).clean()
        if 'Contrasenha' in self.cleaned_data and 'Reingresar_Contrasenha' in self.cleaned_data:
            if self.cleaned_data['Contrasenha'] != self.cleaned_data['Reingresar_Contrasenha']:
                self._errors['Contrasenha'] = [u'Las claves deben coincidir.']
                self._errors['Reingresar_Contrasenha'] = [u'Las claves deben coincidir.']
        return self.cleaned_data

class UsuarioModificadoForm (forms.Form):

    Usuario = forms.CharField(widget=forms.TextInput(), max_length=14, required=True, error_messages={'required': 'Ingrese un nombre de usuario', 'max_length': 'Longitud maxima: 14', 'min_length': 'Longitud minima: 5 caracteres'})
    Contrasenha = forms.CharField(widget=forms.PasswordInput(render_value=False), max_length=14, min_length=5, required=False, error_messages={'required': 'Ingrese contrasenha', 'max_length': 'Longitud maxima: 14', 'min_length': 'Longitu minima: 5 caracteres',})
    Reingresar_Contrasenha = forms.CharField(widget=forms.PasswordInput(render_value=False), max_length=14, min_length=5, required=False, error_messages={'required': 'Ingrese contrasenha', 'max_length': 'Longitud maxima: 14', 'min_length': 'Longitu minima: 5 caracteres',})
    Email = forms.CharField(widget=forms.TextInput(), required=False)
    Nombre = forms.CharField(widget=forms.TextInput(), max_length=30, required=True, error_messages={'required': 'Ingrese nombre', })
    Apellido = forms.CharField(widget=forms.TextInput(), max_length=30, required=True, error_messages={'required': 'Ingrese Apellido', })


    def clean(self):
        super(forms.Form,self).clean()
        if 'contrasenha' in self.cleaned_data and 'reingresarContrasenha' in self.cleaned_data:
            if self.cleaned_data['contrasenha'] != self.cleaned_data['reingresarContrasenha']:
                self._errors['contrasenha'] = [u'Las claves deben coincidir.']
                self._errors['reingresarContrasenha'] = [u'Las claves deben coincidir.']
        return self.cleaned_data
