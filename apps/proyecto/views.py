import datetime
from dateutil.relativedelta import relativedelta
import json
from apps.sprint.models import Sprint

from django.contrib import messages
from django.contrib.auth.models import Group, User
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404, render
from apps.cliente.models import Cliente
from apps.proyecto.forms import ProyectoForm, CambiarEstadoForm, MiembroNuevoForm
from apps.proyecto.models import Proyecto, Equipo
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, permission_required
from apps.user_stories.models import Comentario, User_Story

from skp import settings

__author__ = 'hdmedina'



#@permission_required('proyecto.administrar_proyecto', raise_exception=True)
@login_required
def listar_proyectos(request):

    '''
    vista para listar los proyectos del sistema del sistema junto con el nombre de su lider
    :return render_to_response:
    :param request:
    '''


    proyectos = Proyecto.objects.filter((Q(estado='PEN')|Q(estado='CAN')|Q(estado='ACT')|Q(estado='FIN')))

    proyectosDelUsuario= Equipo.objects.filter(miembro_id = request.user.id)

    try:
        eslider=Proyecto.objects.filter(lider_id=request.user.id)
    except:
        eslider=None

    lider = False
    if eslider:
        lider = True
    if(request.user.is_superuser):
        proyectosDelUsuario= Equipo.objects.all()


    return render_to_response('proyecto/listar_proyectos.html', {'datos': proyectos, 'proyectoDeUsuario': proyectosDelUsuario, 'lider':lider}, context_instance=RequestContext(request))

@login_required
def crear_proyecto(request):
    '''
    Vista para registrar un nuevo proyecto con su lider
    :return render_to_response:
    :param request:
    '''

    if request.method=='POST':
        formulario = ProyectoForm(request.POST)

        if formulario.is_valid():
            if formulario.cleaned_data['fecha_ini']>formulario.cleaned_data['fecha_fin']:
                messages.add_message(request, settings.DELETE_MESSAGE, "Fecha de inicio debe ser menor a la fecha de finalizacion")
            else:
                lider=formulario.cleaned_data['lider']
                #asigna el rol lider al usuario seleccionado
                roles = Group.objects.get(name='Lider')
                lider.groups.add(roles)
                nombre= formulario.cleaned_data['nombre']
                descripcion= formulario.cleaned_data['descripcion']
                fecha_ini=formulario.cleaned_data['fecha_ini']
                fecha_fin=formulario.cleaned_data['fecha_fin']
                duracion_sprint = formulario.cleaned_data['duracion_sprint'] *7
                cliente = formulario.cleaned_data['cliente']
                observaciones = formulario.cleaned_data['observaciones']

                proyectoNvo = Proyecto(nombre = nombre, descripcion = descripcion,
                                       fecha_ini=fecha_ini , fecha_fin=fecha_fin,lider = lider,
                                       duracion_sprint =duracion_sprint , cliente =cliente,
                                       observaciones = observaciones)
                proyectoNvo.save()
                template_name = './proyecto/creacion_correcta.html'
                return render(request, template_name)
    else:
        formulario = ProyectoForm()
    return render_to_response('proyecto/crear_proyecto.html',{'formulario':formulario}, context_instance=RequestContext(request))

def register_success(request):
    """
    Vista de mensaje de crecion correcta de proyecto
    :return render_to_response:
    :param request:
    """
    return render_to_response('proyecto/creacion_correcta.html', context_instance=RequestContext(request))

def detalle_proyecto(request, id_proyecto):
    """
    Vista de visualizacion de detalles de un proyecto
    :return render_to_response:
    :param request:
    :param id_proyecto: id del proyecto del cual se quiere saber el detalle
    """
    dato = get_object_or_404(Proyecto, pk=id_proyecto)
    lider = get_object_or_404(User, pk=dato.lider_id)

    return render_to_response('proyecto/detalle_proyecto.html', {'proyecto': dato,'id_proyecto': id_proyecto, 'lider':lider} )

def abrir_proyecto(request, id_proyecto):
    """
    Vista de visualizacion que se adentra en un proyecto en especifico
    :return render:
    :param request:
    :param id_proyecto: id del proyecto en el cual se quiere entrar
    """
    if(puede_realizar_accion(request.user,id_proyecto,'abrir_proyecto')):
        proyecto = Proyecto.objects.get(pk=id_proyecto)

        # para confirmar si hay Sprint Activo
        try:
            ultimo_sprint = Sprint.objects.filter(proyecto=id_proyecto).order_by('-numero')[0]
        except :
            ultimo_sprint = None

        # para que no explote en el caso trivial de cuando no hay ningun sprint aun
        if ultimo_sprint is not None:

            # la fecha de fin en formato Date
            fin = datetime.datetime.strptime(str(ultimo_sprint.fecha_fin), "%Y-%m-%d")

            hoy = datetime.datetime.now()  # fecha actual
            hoyFormat = hoy.strftime("%Y-%m-%d")

            # paso a formato Date la fecha de hoy
            d3 = datetime.datetime.strptime(str(hoyFormat), "%Y-%m-%d")

            if ultimo_sprint.estado == 'ACT':
                if d3 > fin:                        # quiere decir que ya termino
                    ultimo_sprint.estado = 'FIN'
                    ultimo_sprint.save()

                    user_story = ultimo_sprint.user_story.all()

                    for us in user_story:
                        us.tiene_sprint = False     # ya no esta en un sprint
                        us.save()

        return render_to_response('proyecto/abrir_proyecto.html', {'proyecto': proyecto, 'id_proyecto': id_proyecto}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/denegado/')
def buscar_proyectos(request):
    """
    vista para buscar los proyectos del sistema
    :return render_to_response:
    :param request:
    """
    query = request.GET.get('q', '')
    if query:
        qset = (
            Q(nombre=query)
        )
        results = Proyecto.objects.filter(qset).distinct()

    else:
        results = Proyecto.objects.all().order_by('nombre')


    return render_to_response('proyecto/listar_proyectos.html', {'datos': results}, context_instance=RequestContext(request))

def editar_proyecto(request,id_proyecto):
    """
    Vista para editar un proyecto, se pueden editar los campos dependiendo del permiso que se posee
    :return render_to_response:
    :return render:
    :param request:
    :param id_proyecto: id del proyecto que se quiere editar
    """
    proyecto= Proyecto.objects.get(id=id_proyecto)
    nombre= proyecto.nombre
    if proyecto.estado=='PEN':
        return HttpResponseRedirect ('/denegado')
    if request.method == 'POST':
        # formulario enviado
        proyecto_form = ProyectoForm(request.POST, instance=proyecto)
        if proyecto_form.is_valid():
            if proyecto_form.cleaned_data['fecha_ini']>proyecto_form.cleaned_data['fecha_fin']:
                messages.add_message(request, settings.DELETE_MESSAGE, "Fecha de inicio debe ser menor a la fecha de finalizacion")
            else:
                lider=proyecto_form.cleaned_data['lider']
                roles = Group.objects.get(name='Lider')
                lider.groups.add(roles)
            # formulario validado correctamente
                proyecto_form.save()

                template_name = './proyecto/proyecto_modificado.html'
                return render(request, template_name, {'id_proyecto': id_proyecto})

    else:
        # formulario inicial
        proyecto_form = ProyectoForm(instance=proyecto)
    return render_to_response('proyecto/editar_proyecto.html', { 'proyecto': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))



def cambiar_estado(request,id_proyecto):
    proyecto= Proyecto.objects.get(id=id_proyecto)
    proyectos = Proyecto.objects.filter((Q(estado='PEN')|Q(estado='CAN')|Q(estado='ACT')|Q(estado='FIN')))

    if puede_realizar_accion(request.user,id_proyecto,'cambiar_estado_proyecto'):
        if request.method=='POST':
            formulario = CambiarEstadoForm(request.POST)
            if formulario.is_valid():
                formulario.clean()
                estado = formulario.cleaned_data['estado']

                proyectoModificado = Proyecto(nombre=proyecto.nombre,
                                              id = proyecto.id,
                                              descripcion=proyecto.descripcion,
                                              fecha_ini=proyecto.fecha_ini,
                                              fecha_fin= proyecto.fecha_fin,
                                              duracion_sprint = proyecto.duracion_sprint,
                                              fecha_fin_real = proyecto.fecha_fin_real,
                                              estado= estado,
                                              lider_id = proyecto.lider_id,
                                              observaciones = proyecto.observaciones,
                                              cliente_id = proyecto.cliente_id
                                              )
                proyectoModificado.save()

                return HttpResponseRedirect('/adm_proyectos/', {'id_proyecto': id_proyecto, 'datos':proyectos})

        else:
            # formulario inicial
            formulario = CambiarEstadoForm(instance=proyecto)
        return render_to_response('proyecto/editar_proyecto.html', { 'proyecto': formulario, 'nombre':proyecto.nombre}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/denegado/')



def eliminacion_logica(request, id_proyecto):
    """
    Vista para eliminar un proyecto. La eliminacion se hace de forma logica
    :return HttpResponseRedirect:
    :param request:
    :param id_proyecto: id del proyecto que se quiere eliminar
    """
    if puede_realizar_accion(request.user,id_proyecto,'eliminar_proyecto'):
        eliminacionLogica = Proyecto.objects.get(pk=id_proyecto)
        eliminacionLogica.borrado_logico = True
        eliminacionLogica.save()
        return HttpResponseRedirect('/adm_proyectos')
    else:
        return HttpResponseRedirect('/denegado/')

def administrarEquipo(request, id_proyecto):
    """
    Vista para administrar el equipo de un proyecto. El equipo consiste en un conjunto de usuarios
    con sus respectivos roles dentro del proyecto.
    :return HttpResponseRedirect:
    :param request:
    :param id_proyecto: id del proyecto al que corresponde el equipo

    :author: Eduardo Mendez
    """
    usuario_actor = request.user
    equipo = Equipo.objects.filter(proyecto= id_proyecto).order_by('id')

    ctx = {'usuario_actor':usuario_actor,'equipo':equipo, 'id_proyecto':id_proyecto}
    return render_to_response('proyecto/equipo/equipo.html', ctx, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def agregarMiembro(request,id_proyecto):
    """
    Vista para agregar miembros al equipo de un proyecto. En el se guardan los datos del ID del usuario que sera
    parte del equipo y el ID del rol que ejerce en el proyecto.
    :return HttpResponseRedirect:
    :param request:
    :param id_proyecto: id del proyecto al que corresponde el equipo

    :author: Eduardo Mendez
    """

    usuario_actor = request.user
    proyecto = Proyecto.objects.filter(id=id_proyecto)
    equipo = Equipo.objects.filter(proyecto = id_proyecto)
    if request.method == 'POST':
        form = MiembroNuevoForm(request.POST)

        if form.is_valid():                             #verfica si tiene errores el formulario
            form.clean()
            usuario = form.cleaned_data['Usuario']
            rol = form.cleaned_data['Rol']


            miembroNuevo = Equipo(miembro_id=usuario.id, rol_id=rol.id, proyecto_id=id_proyecto)
            miembroNuevo.save()


            template_name = './proyecto/equipo/miembro_agregado.html'
            return render(request, template_name, {'id_proyecto': id_proyecto})


    else:
        form = MiembroNuevoForm()

    template_name = './proyecto/equipo/miembro_nuevo.html'
    return render(request, template_name, {'form': form})


@login_required(login_url='/login/')
def confQuitarMiembro(request, id_equipo,id_proyecto):
    """
    Recibe un request, la clave del flujo y la clave del proyecto por el cual se accedio,
    para confirmar el borrado logico del flujo en caso de ser posible.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_equipo : integer.
    :param id_equipo : Contiene el id del miembro que sera eliminado.

    :type id_proyecto : integer.
    :param id_proyecto : Contiene el id del proyecto al que corresponde.

    :rtype: django.http.HttpResponse
    :return:

    :author: Eduardo Mendez
    """
    template_name = './proyecto/equipo/conf_quitar_miembro.html'
    miembro = Equipo.objects.get(pk=id_equipo)

    return render(request, template_name, {'miembro': miembro, 'id_proyecto':id_proyecto})

@login_required(login_url='/login/')
def quitarMiembro(request, id_equipo,id_proyecto):
    """
    Recibe un request, el id del proyecto y el id del miembro que sera eliminado del equipo,
    para luego mostrar el formulario de eliminacion.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_equipo : integer.
    :param id_equipo : Contiene el id del miembro del equipo.

    :type id_proyecto : integer.
    :param id_proyecto : Contiene el id del proyecto del cual es miembro.

    :rtype: django.http.HttpResponse
    :return:

    :author: Eduardo Mendez
    """
    miembro = Equipo.objects.get(pk=id_equipo)
    miembro.delete()

    template_name = './proyecto/equipo/quitar_miembro.html'
    return render(request, template_name, {'id_proyecto': id_proyecto})

def puede_realizar_accion(user,id_proyecto,permiso):
    #print(permiso)
    #print(id_proyecto)
    proyecto = Proyecto.objects.get(pk=id_proyecto)
    eq = Equipo.objects.filter(proyecto = proyecto , miembro = user)
    #print(len(eq))

    if user.is_superuser==True:
        return True

    if user == proyecto.lider:
        return True

    elif not eq==0:
        for miemb in eq:
            if miemb.rol.permissions.filter(Q(codename = permiso)):
                return True
            else:
                return False
