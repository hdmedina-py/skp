__author__ = 'miguel'

from django.db import models
from apps.user_stories.models import User_Story
from apps.proyecto.models import Proyecto

ESTADOS = (

    ('ACT', 'Activo'),
    ('FIN','Finalizado')
)

class Sprint(models.Model):
    """
    Contiene los campos de la tabla Sprint en la Base de Datos.
    El ID del Sprint.
    Fecha Inicio
    Fecha Fin
    Finalizado

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Miguel Pereira
    """

    numero = models.IntegerField()
    fecha_ini= models.DateField(null=False)
    fecha_fin= models.DateField(null=False)
    estado = models.CharField(max_length=3, choices= ESTADOS)
    proyecto = models.ForeignKey(Proyecto)
    user_story = models.ManyToManyField(User_Story)

    def __unicode__(self):
        return unicode(self.numero)

    class Meta:
        verbose_name = 'sprint'
        verbose_name_plural = 'sprints'
        permissions = (
            ('iniciar_sprint', 'Puede iniciar sprint'),
            ('listar_sprint','Puede listar sprint'),
        )
